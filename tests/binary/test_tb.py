from os import path
from shutil import copytree
import os
import subprocess, sys


def test_local_plugins(tmp):

    local_project_dir = path.join(path.dirname(path.dirname(path.realpath(__file__))),
                                  "project_dir_with_plugin")
    project_dir = path.join(tmp.dir, "project")
    copytree(local_project_dir, project_dir)

    tb_bin = path.join(os.getcwd(), "dist/tb")
    output = run_cmd(project_dir, tb_bin)
    assert "echo" in output


def run_cmd(cwd, *args):
    from subprocess import check_output
    out: bytes = check_output(args, cwd=cwd)

    return out.decode("utf8")