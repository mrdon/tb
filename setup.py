import runpy
from os.path import splitext, basename, join, abspath, dirname

from setuptools import setup, find_packages, findall
from setuptools.glob import glob

here = abspath(dirname(__file__))


def get_version():
    file_globals = runpy.run_path("src/tb/version.py")
    return file_globals['__version__']


def read(*parts):
    with open(join(here, *parts), 'r') as f:
        return f.read()


LONG_DESCRIPTION = read('README.md')
REQUIREMENTS = read('requirements.txt')

setup(
    name='tb-cli',
    version=get_version(),
    description='The team b cli',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Don Brown',
    author_email='mrdon@twdata.org',
    url='https://bitbucket.org/mrdon/tb',
    license='aplv2',
    install_requires=REQUIREMENTS,
    packages=find_packages('src'),
    package_dir={'': 'src'},
    py_modules=[splitext(basename(path))[0] for path in glob('src/*.py')],
    package_data={'tb': ['templates/*'], '': ['requirements.txt']},
    data_files=[('tb/plugins.d', findall("plugins.d")),
                ('tb/plugins/repo', findall("plugins/repo")),
                ('tb/plugins/devloop', findall("plugins/devloop"))],
    include_package_data=True,
    entry_points="""
        [console_scripts]
        tb = tbmain:main
    """,
    python_requires='>=3.7',
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Internet",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3 :: Only",
    ],
)
