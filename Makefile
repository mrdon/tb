REQUIREMENTS_FILE := requirements-dev.txt
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
    REQUIREMENTS_FILE := requirements-osx.txt
endif

.PHONY: clean virtualenv test dist default dist-osx-upload help

# Help system from https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

ci-test: clean virtualenv test dist ;

clean:
	find . -name '*.py[co]' -delete

virtualenv:
	python3.7 -m venv venv
	venv/bin/pip install pip==18.1
	venv/bin/pip install -r $(REQUIREMENTS_FILE)
	venv/bin/pip install -r ci/requirements.txt
	venv/bin/pip install -e .
	@echo
	@echo "VirtualENV Setup Complete. Now run: source venv/bin/activate"
	@echo

install: virtualenv
	sudo ln -s venv/bin/tb /usr/local/bin

test: test-unit test-binary  ## Run all tests

test-unit:  ## Run unit tests locally
	venv/bin/python -m pytest \
		-v \
		--cov=tb \
		--cov-report=term \
		--cov-report=html:coverage-report \
		tests/unit \
		plugins/


test-binary: dist  ## Run binary tests locally
	venv/bin/python -m pytest \
		-v \
		tests/binary

test-docker: ## Run tests in a clean docker container
	docker run --rm -it -v `pwd`:/app:rw -w /app python:3.7 make ci-test

dist: clean ## Builds a tb binary
	rm -rf dist/*
	venv/bin/pyinstaller tbmain.spec --onefile
	dist/tb -v

dist-osx-upload: clean virtualenv ## Builds and releases an osx file
	source venv/bin/activate
	ci/local-osx-release.sh
	deactivate


